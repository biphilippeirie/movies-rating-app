import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faHome, faBars, faIdCard } from "@fortawesome/free-solid-svg-icons";
import {
  faLinkedinIn,
  faLinkedin,
  faGoogle
} from "@fortawesome/free-brands-svg-icons";
import * as vueFontawesome from "@fortawesome/vue-fontawesome";

library.add(faHome, faBars, faIdCard, faLinkedin, faLinkedinIn, faGoogle);

Vue.component("font-awesome-icon", vueFontawesome.FontAwesomeIcon);
Vue.component("font-awesome-layers", vueFontawesome.FontAwesomeLayers);
Vue.component("font-awesome-layers-text", vueFontawesome.FontAwesomeLayersText);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
